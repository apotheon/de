compiler      = clang
de_use_stdlib = false
flags	      = -I. -Os -std=c89 -Wall -Wextra -Werror -pedantic
name	      = de

ifeq ($(de_use_stdlib), true)
	override flags += -DdeUseStandardLibrary
endif

build: lib main.c
	@echo "Building De..."
	@$(compiler) $(flags) -L. -l$(name) $(name).c main.c -o $(name)
	@echo "Finished building De."

lib: clean $(name).c $(name).h
	@echo "Building libde..."
	@$(compiler) -c $(flags) $(name).c
	@ar rcs lib$(name).a $(name).o
	@rm -rf $(name).o
	@echo "Finished building libde."

clean:
	@echo "Cleaning..."
	@rm -rf lib$(name).a
	@rm -rf $(name)
	@echo "Finished cleaning."
