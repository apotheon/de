#define deNil ((void*)0)
typedef struct   deStack       deStack;
typedef struct   dePopped      dePopped;
typedef struct   deWord        deWord;
typedef struct   deInterpreter deInterpreter;
typedef unsigned char          deBool;
enum {
    false = 0,
    true  = !false
};

struct deStack {
    unsigned int* Stack;
    int           Top;
    int           Size;
};
struct dePopped {
    int     Value;
    deStack Stack;
};
struct deWord {
    char*         Name;
    deWord*       Bank;
    unsigned int  Index;
    unsigned int  Size;
    deBool        Temporary;
    deStack       (*Runtime)(deStack);
    deInterpreter (*Compile)(deInterpreter);
    char**        Code;
};
struct deInterpreter {
    deWord  Dictionary;
    deStack Stack;
    deStack Temporary;
    char**  Program;
    deBool  Running;
};

extern void* (*deAllocate)(unsigned long);
extern void  (*deRelease) (void*);




/*
    General library functions.
*/

deBool Initialize(void* (*allocate)(unsigned long),
                  void  (*release) (void*));
void*  deMemoryCopy(void* data, unsigned int size);



/*
    deStack functions.
*/

deStack  dePush     (unsigned int value, deStack stack);
dePopped dePop      (deStack stack);
deStack  dePick     (deStack stack);
deStack  deRoll     (deStack stack);
deStack  deDrop     (deStack stack);
deStack  deDuplicate(deStack stack);
deStack  deOver     (deStack stack);
deStack  deSwap     (deStack stack);
deStack  deRotate   (deStack stack);
deStack  deAdd      (deStack stack);
deStack  deSubtract (deStack stack);
deStack  deMultiply (deStack stack);
deStack  deDivide   (deStack stack);
deStack  deAND      (deStack stack);
deStack  deOR       (deStack stack);
deStack  deNOT      (deStack stack);




/*
    String functions.
*/

unsigned int deStringLength     (char* data);
char*        deStringCopy       (char* string);
deBool       deStringCompare    (char* string1, char* string2);
char*        deNullifyWhitespace(char* string);
unsigned int deCountTokens      (char* string, unsigned int length);
char**       deTokenizeString   (char* string);
unsigned int deTokensLength     (char** tokens);
char**       deTokensAppend     (char* string, char** tokens);
deBool       deIsNumeric        (char* string);
int          deToNumeric        (char* string);




/*
    deWord functions.
*/

deWord       deDefine(char*         name,
                      deBool        temporary,
                      deStack       (*runtime)(deStack),
                      deInterpreter (*compile)(deInterpreter),
                      char**        code,
                      deWord        dictionary);
deWord       deFind            (char* name, deWord dictionary);
deWord       deForget          (char* name, deWord dictionary);
deWord       deDeleteDictionary(deWord dictionary);




/*
    deInterpreter functions.
*/

deInterpreter deNewInterpreter(deWord*       bank,
                               unsigned int  banksize,
                               unsigned int* stack,
                               int           stacksize,
                               unsigned int* temporary,
                               int           temporarysize,
                               char**        program);
deInterpreter deExit               (deInterpreter interpreter);
deInterpreter deComment            (deInterpreter interpreter);
deInterpreter deCompileWord        (deInterpreter interpreter);
deInterpreter deIf                 (deInterpreter interpreter);
deInterpreter deDefineStandardWords(deInterpreter interpreter);
deInterpreter deExecute            (deInterpreter interpreter);
deInterpreter deRunProgram         (char* program, deInterpreter interpreter);
deInterpreter deDeleteInterpreter  (deInterpreter interpreter);
