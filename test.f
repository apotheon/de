def greater-than-one
    dup 1 >
end

def decrement
    1 -
end

def factorial
    greater-than-one if
        dup decrement
        factorial *
    else then
end

10 factorial

( Result is 3628800. )
