#include <stdio.h>
#include "de.h"

deWord        WordBank       [100];
unsigned int  StackMemory    [100];
unsigned int  TempStackMemory[100];

deInterpreter Run(char* input, deInterpreter interpreter) {
    int iterator;
    interpreter = deRunProgram(input, interpreter);
    for(iterator = 0; iterator < interpreter.Stack.Top; iterator++) {
        printf("%d ", interpreter.Stack.Stack[iterator]);
    }
    printf("\n");
    return interpreter;
}

void REPL() {
    char*         input;
    deInterpreter interpreter;
    interpreter = deNewInterpreter(WordBank,        100,
                                   StackMemory,     100,
                                   TempStackMemory, 100,
                                   deNil);
    input       = deAllocate(sizeof(char) * 1024);
    interpreter = deDefineStandardWords(interpreter);
    while(interpreter.Running) {
        printf("> ");
        fgets(input, sizeof(char) * 2048, stdin);
        interpreter = Run(input, interpreter);
    }
    interpreter = deDeleteInterpreter(interpreter);
    deRelease(input);
    return;
}

int ExecuteFile(char** argv) {
    FILE*         file;
    char*         input;
    unsigned int  iterator;
    deInterpreter interpreter;
    file = fopen(argv[1], "r");
    if(file == deNil) {
        printf("Error: \"%s\" not found.\n", argv[1]);
        return 3;
    }
    fseek(file, 0, SEEK_END);
    input = deAllocate(sizeof(char) * (ftell(file) + 1));
    iterator        = ftell(file);
    input[iterator] = '\0';
    fseek(file, 0, SEEK_SET);
    fread(input, iterator, sizeof(char), file);
    fclose(file);
    interpreter = deNewInterpreter(WordBank,        100,
                                   StackMemory,     100,
                                   TempStackMemory, 100,
                                   deNil);
    interpreter = deDefineStandardWords(interpreter);
    interpreter = Run(input, interpreter);
    interpreter = deDeleteInterpreter(interpreter);
    deRelease(input);
    return 0;
}

void ExecuteArgV(char** argv) {
    char*         input;
    deInterpreter interpreter;
    input       = deStringCopy(argv[2]);
    interpreter = deNewInterpreter(WordBank,        100,
                                   StackMemory,     100,
                                   TempStackMemory, 100,
                                   deNil);
    interpreter = deDefineStandardWords(interpreter);
    interpreter = Run(input, interpreter);
    interpreter = deDeleteInterpreter(interpreter);
    deRelease(input);
    return;
}

int main(int argc, char** argv) {
    if(argc == 3) {
        if(deStringCompare("-e", argv[1]) == true) {
            ExecuteArgV(argv);
        }
        else {
            printf("Usage:\n    ./de <file>\n    ./de -e <code>\n    ./de -r");
            printf("\n");
            return 1;
        }
    }
    else if(argc == 2) {
        if(deStringCompare("-r", argv[1]) == true) {
            REPL();
        }
        else {
            return ExecuteFile(argv);
        }
    }
    else {
        printf("Usage:\n    ./de <file>\n    ./de -e <code>\n    ./de -r");
        printf("\n");
        return 2;
    }
    return 0;
}
