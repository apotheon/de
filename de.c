#include "de.h"
#include <stdio.h>

#ifdef deUseStandardLibrary
    #include <stdlib.h>
    void* (*deAllocate)(unsigned long) = malloc;
    void  (*deRelease) (void*)         = free;
#else
    void* (*deAllocate)(unsigned long) = deNil;
    void  (*deRelease) (void*)         = deNil;
#endif




/*
    General library functions.
*/

deBool deInitialize(void* (*allocate)(unsigned long),
                    void  (*release) (void*)) {
    if(allocate == deNil ||
       release  == deNil) {
        return false;
    }
    else {
        deAllocate = allocate;
        deRelease  = release;
        return true;
    }
}

void* deMemoryCopy(void* data, unsigned int size) {
    unsigned char* result;
    unsigned char* temporary;
    if(data == deNil ||
       size == 0) {
        return deNil;
    }
    result = deAllocate(sizeof(unsigned char) * size);
    if(result == deNil) {
        return data;
    }
    temporary = result;
    do {
        *((unsigned char*)result) = *((unsigned char*)data);
        data                      = ((unsigned char*)data) + 1;
        result++;
        size--;
    } while(size != 0);
    return temporary;
}




/*
    deStack functions.
*/

deStack dePush(unsigned int value, deStack stack) {
    if((stack.Top + 1) <  0          ||
       stack.Top       >= stack.Size ||
       stack.Stack     == deNil) {
        return stack;
    }
    stack.Stack[stack.Top] = value;
    stack.Top++;
    return stack;
}

dePopped dePop(deStack stack) {
    dePopped result;
    if((stack.Top - 1) <  0 ||
       stack.Stack     == deNil) {
        result.Value = 0;
        result.Stack = stack;
        return result;
    }
    stack.Stack[stack.Top] = 0;
    stack.Top--;
    result.Value = stack.Stack[stack.Top];
    result.Stack = stack;
    return result;
}

deStack dePick(deStack stack) {
    dePopped operand;
    if(stack.Stack == deNil) {
        stack.Top = 0;
        return stack;
    }
    operand = dePop(stack);
    if(operand.Stack.Top - 1 < operand.Value) {
        return operand.Stack;
    }
    #define Offset ((operand.Stack.Top - 1) - operand.Value)
    return dePush(operand.Stack.Stack[Offset], operand.Stack);
    #undef Offset
}

deStack deRoll(deStack stack) {
    dePopped     operand;
    unsigned int temporary;
    int          iterator;
    if(stack.Stack == deNil) {
        stack.Top = 0;
        return stack;
    }
    operand = dePop(stack);
    #define Offset ((operand.Stack.Top - 1) - operand.Value)
    if(Offset < 0) {
        return operand.Stack;
    }
    temporary = operand.Stack.Stack[Offset];
    for(iterator = Offset; iterator < operand.Stack.Top; iterator++) {
        operand.Stack.Stack[iterator] = operand.Stack.Stack[iterator + 1];
    }
    #undef Offset
    operand.Stack.Stack[(operand.Stack.Top - 1)] = temporary;
    return operand.Stack;
}

deStack deDrop(deStack stack) {
    return dePop(stack).Stack;
}

deStack deDuplicate(deStack stack) {
    return dePick(dePush(0, stack));
}

deStack deOver(deStack stack) {
    return dePick(dePush(1, stack));
}

deStack deSwap(deStack stack) {
    return deRoll(dePush(1, stack));
}

deStack deRotate(deStack stack) {
    return deRoll(dePush(2, stack));
}

deStack deAdd(deStack stack) {
    dePopped operandA;
    dePopped operandB;
    operandA = dePop(stack);
    operandB = dePop(operandA.Stack);
    return dePush(operandB.Value + operandA.Value, operandB.Stack);
}

deStack deSubtract(deStack stack) {
    dePopped operandA;
    dePopped operandB;
    operandA = dePop(stack);
    operandB = dePop(operandA.Stack);
    return dePush(operandB.Value - operandA.Value, operandB.Stack);
}

deStack deMultiply(deStack stack) {
    dePopped operandA;
    dePopped operandB;
    operandA = dePop(stack);
    operandB = dePop(operandA.Stack);
    return dePush(operandB.Value * operandA.Value, operandB.Stack);
}

deStack deDivide(deStack stack) {
    dePopped operandA;
    dePopped operandB;
    operandA = dePop(stack);
    operandB = dePop(operandA.Stack);
    return (operandA.Value == 0) ?
           operandB.Stack        :
           dePush(operandB.Value / operandA.Value, operandB.Stack);
}

deStack deAND(deStack stack) {
    dePopped operandA;
    dePopped operandB;
    operandA = dePop(stack);
    operandB = dePop(operandA.Stack);
    return dePush(operandB.Value && operandA.Value, operandB.Stack);
}

deStack deOR(deStack stack) {
    dePopped operandA;
    dePopped operandB;
    operandA = dePop(stack);
    operandB = dePop(operandA.Stack);
    return dePush(operandB.Value || operandA.Value, operandB.Stack);
}

deStack deNOT(deStack stack) {
    dePopped operand;
    operand = dePop(stack);
    return dePush(!operand.Value, operand.Stack);
}

deStack deLessThan(deStack stack) {
    dePopped operandA;
    dePopped operandB;
    operandA = dePop(stack);
    operandB = dePop(operandA.Stack);
    return dePush(operandB.Value < operandA.Value, operandB.Stack);
}

deStack deGreaterThan(deStack stack) {
    dePopped operandA;
    dePopped operandB;
    operandA = dePop(stack);
    operandB = dePop(operandA.Stack);
    return dePush(operandB.Value > operandA.Value, operandB.Stack);
}

deStack deLessThanEq(deStack stack) {
    dePopped operandA;
    dePopped operandB;
    operandA = dePop(stack);
    operandB = dePop(operandA.Stack);
    return dePush(operandB.Value <= operandA.Value, operandB.Stack);
}

deStack deGreaterThanEq(deStack stack) {
    dePopped operandA;
    dePopped operandB;
    operandA = dePop(stack);
    operandB = dePop(operandA.Stack);
    return dePush(operandB.Value >= operandA.Value, operandB.Stack);
}

deStack deEquals(deStack stack) {
    dePopped operandA;
    dePopped operandB;
    operandA = dePop(stack);
    operandB = dePop(operandA.Stack);
    return dePush(operandB.Value == operandA.Value, operandB.Stack);
}



/*
    dedeString functions.
*/

unsigned int deStringLength(char* data) {
    char* result;
    if(data == deNil) {
        return 0;
    }
    result = data;
    while(*result++ != '\0');
    return (result - data);
}

char* deStringCopy(char* string) {
    return deMemoryCopy(string, deStringLength(string));
}

deBool deStringCompare(char* string1, char* string2) {
    if(string1                 == deNil ||
       string2                 == deNil ||
       deStringLength(string1) != deStringLength(string2)) {
        return false;
    }
    if(string1 == string2) {
        return true;
    }
    else {
        while(*string1 != '\0' && *string2 != '\0') {
            if(*string1 != *string2) {
                return false;
            }
            string1++;
            string2++;
        }
        return true;
    }
}

char* deNullifyWhitespace(char* string) {
    char* temporary;
    if(string == deNil) {
        return deNil;
    }
    temporary = string;
    while(*string != '\0') {
        if(*string == ' '  ||
           *string == '\n' ||
           *string == '\t') {
            *string = '\0';
        }
        string++;
    }
    return temporary;
}

unsigned int deCountTokens(char* string, unsigned int length) {
    unsigned int result;
    char*        temporary;
    if(string == deNil) {
        return 0;
    }
    result    = 0;
    temporary = string;
    do {
        if(*string != '\0') {
            result++;
        }
        string += deStringLength(string);
    } while((string - temporary) != length);
    return result;
}

char** deTokenizeString(char* string) {
    unsigned int length;
    char**       result;
    char**       temporary;
    if(string == deNil) {
        return deNil;
    }
    length = deStringLength(string);
    if(length == 1) {
        return deNil;
    }
    string = deNullifyWhitespace(deStringCopy(string));
    if(string == deNil) {
        return deNil;
    }
    if(deCountTokens(string, length) == 0) {
        deRelease(string);
        return deNil;
    }
    result = deAllocate(sizeof(char*) * (deCountTokens(string, length) + 1));
    if(result == deNil) {
        return deNil;
    }
    length    = deCountTokens(string, length);
    temporary = result;
    while((result - temporary) < length) {
        if(*string != '\0') {
            *result = string;
            result++;
        }
        string += deStringLength(string);
    }
    *result = deNil;
    return temporary;
}

unsigned int deTokensLength(char** tokens) {
    char** result;
    if(tokens == deNil) {
        return 0;
    }
    result = tokens;
    while(*result++ != deNil);
    return (result - tokens);
}

char** deTokensAppend(char* string, char** tokens) {
    char** result;
    char** temporary;
    if(tokens == deNil) {
        if(string == deNil) {
            return deNil;
        }
        result = deAllocate(sizeof(char*) * 2);
        if(result == deNil) {
            return deNil;
        }
        result[0] = string;
        result[1] = deNil;
        return result;
    }
    result = deAllocate(sizeof(deWord*) * (deTokensLength(tokens) + 1));
    if(result == deNil) {
        return deNil;
    }
    result[deTokensLength(tokens) - 1] = deNil;
    temporary = tokens;
    while(*tokens != deNil) {
        result[(tokens - temporary)] = *tokens;
        tokens++;
    }
    result[(tokens - temporary)]     = string;
    result[(tokens - temporary) + 1] = deNil;
    deRelease(temporary);
    return result;
}

deBool deIsNumeric(char* string) {
    char* temporary;
    if(string == deNil) {
        return false;
    }
    temporary = string;
    while(*string != '\0') {
        if(*string < '0' || *string > '9') {
            if((string - temporary) == 0 && *string == '-') {
                string++;
                continue;
            }
            else {
                return false;
            }
        }
        string++;
    }
    return true;
}

int deToNumeric(char* string) {
    int  result;
    deBool sign;
    if(string              == deNil ||
       deIsNumeric(string) == false) {
        return 0;
    }
    result = 0;
    sign   = false;
    if(*string == '-') {
        sign = true;
        string++;
    }
    while(*string != '\0') {
        result = (result * 10) + (*string - '0');
        string++;
    }
    return (sign) ?
           result * -1 :
           result;
}




/*
    deWord functions.
*/

deWord deDefine(char*         name,
                deBool        temporary,
                deStack       (*runtime)(deStack),
                deInterpreter (*compile)(deInterpreter),
                char**        code,
                deWord        dictionary) {
    if(dictionary.Index >= dictionary.Size ||
       dictionary.Bank  == deNil           ||
       name             == deNil) {
        return dictionary;
    }
    dictionary.Bank[dictionary.Index]      = dictionary;
    dictionary.Bank[dictionary.Index].Name = deStringCopy(name);
    if(dictionary.Bank[dictionary.Index].Name == deNil) {
        return dictionary;
    }
    dictionary.Bank[dictionary.Index].Temporary = temporary;
    dictionary.Bank[dictionary.Index].Runtime   = runtime;
    dictionary.Bank[dictionary.Index].Compile   = compile;
    dictionary.Bank[dictionary.Index].Code      = code;
    dictionary.Index++;
    return dictionary;
}

deWord deFind(char* name, deWord dictionary) {
    unsigned int iterator;
    if(dictionary.Bank  == deNil ||
       dictionary.Index == 0) {
        return dictionary;
    }
    for(iterator = (dictionary.Index - 1); iterator != 0; iterator--) {
        if(deStringCompare(name, dictionary.Bank[iterator].Name) == true) {
            break;
        }
    }
    if(iterator                                            == 0 &&
       deStringCompare(name, dictionary.Bank[iterator].Name) == false) {
        return dictionary;
    }
    return dictionary.Bank[iterator];
}

deWord deForget(char* name, deWord dictionary) {
    unsigned int iterator;
    unsigned int temporary;
    if(dictionary.Bank == deNil ||
       deRelease       == deNil) {
        return dictionary;
    }
    for(iterator = (dictionary.Index - 1); iterator != 0; iterator--) {
        if(deStringCompare(name, dictionary.Bank[iterator].Name) == true) {
            break;
        }
    }
    if(iterator                                              == 0 &&
       deStringCompare(name, dictionary.Bank[iterator].Name) == false) {
        return dictionary;
    }
    deRelease(dictionary.Bank[iterator].Name);
    temporary = iterator;
    while(iterator <= dictionary.Index) {
        dictionary.Bank[iterator] = dictionary.Bank[iterator + 1];
        iterator++;
    }
    dictionary.Index--;
    iterator = temporary;
    while(iterator <= dictionary.Index - 1) {
        dictionary.Bank[iterator].Index--;
        iterator++;
    }
    return dictionary;
}

deWord deDeleteDictionary(deWord dictionary) {
    char** temporary;
    if(dictionary.Bank == deNil ||
       deRelease       == deNil) {
        return dictionary;
    }
    dictionary.Index = 0;
    while(dictionary.Bank[dictionary.Index].Name != deNil) {
        deRelease(dictionary.Bank[dictionary.Index].Name);
        dictionary.Bank[dictionary.Index].Name = deNil;
        if(dictionary.Bank[dictionary.Index].Code != deNil) {
            temporary = dictionary.Bank[dictionary.Index].Code;
            while(*dictionary.Bank[dictionary.Index].Code != deNil) {
                deRelease(*dictionary.Bank[dictionary.Index].Code);
                dictionary.Bank[dictionary.Index].Code++;
            }
            deRelease(temporary);
        }
        dictionary.Index++;
    }
    dictionary.Index = 0;
    dictionary.Bank  = deNil;
    return dictionary;
}




/*
    deInterpreter functions.
*/

deInterpreter deNewInterpreter(deWord*       bank,
                               unsigned int  banksize,
                               unsigned int* stack,
                               int           stacksize,
                               unsigned int* temporary,
                               int           temporarysize,
                               char**        program) {
    deInterpreter result;
    if(bank  == deNil ||
       stack == deNil) {
        result.Dictionary.Name      = deNil;
        result.Dictionary.Bank      = deNil;
        result.Dictionary.Index     = 0;
        result.Dictionary.Size      = 0;
        result.Dictionary.Temporary = false;
        result.Dictionary.Runtime   = deNil;
        result.Dictionary.Compile   = deNil;
        result.Dictionary.Code      = deNil;
        result.Stack.Stack          = deNil;
        result.Stack.Top            = 0;
        result.Stack.Size           = 0;
        result.Temporary            = result.Stack;
        result.Program              = deNil;
        result.Running              = true;
        return result;
    }
    result.Dictionary.Name      = deNil;
    result.Dictionary.Bank      = bank;
    result.Dictionary.Index     = 0;
    result.Dictionary.Size      = banksize;
    result.Dictionary.Temporary = false;
    result.Dictionary.Runtime   = deNil;
    result.Dictionary.Compile   = deNil;
    result.Dictionary.Code      = deNil;
    result.Stack.Stack          = stack;
    result.Stack.Top            = 0;
    result.Stack.Size           = stacksize;
    result.Temporary            = result.Stack;
    result.Temporary.Stack      = temporary;
    result.Temporary.Size       = temporarysize;
    result.Program              = program;
    result.Running              = true;
    return result;
}

deInterpreter deExit(deInterpreter interpreter) {
    interpreter.Running = false;
    return interpreter;
}

deInterpreter deComment(deInterpreter interpreter) {
    char** temporary;
    if(interpreter.Program == deNil) {
        return interpreter;
    }
    temporary = interpreter.Program;
    while(*interpreter.Program                       != deNil &&
          deStringCompare(")", *interpreter.Program) == false) {
        interpreter.Program++;
    }
    if(*interpreter.Program == deNil) {
        interpreter.Program = deNil;
        return interpreter;
    }
    while(deStringCompare(")", *interpreter.Program) == false) {
        interpreter.Program++;
    }
    return interpreter;
}

deInterpreter deCompileWord(deInterpreter interpreter) {
    char** temporary;
    char** code;
    if(interpreter.Program         == deNil ||
       interpreter.Dictionary.Bank == deNil) {
        return interpreter;
    }
    temporary = interpreter.Program;
    while(*interpreter.Program                         != deNil &&
          deStringCompare("end", *interpreter.Program) == false) {
        interpreter.Program++;
    }
    if(*interpreter.Program == deNil) {
        interpreter.Program = deNil;
        return interpreter;
    }
    interpreter.Program = temporary + 2;
    code                = deNil;
    while(deStringCompare("end", *interpreter.Program) == false) {
        code = deTokensAppend(deStringCopy(*interpreter.Program), code);
        if(code[deTokensLength(code) - 2] == deNil) {
            interpreter.Program = deNil;
            return interpreter;
        }
        interpreter.Program++;
    }
    interpreter.Dictionary = deDefine(*(temporary + 1),
                                      false,
                                      deNil,
                                      deNil,
                                      code,
                                      interpreter.Dictionary);
    return interpreter;
}

deInterpreter deIf(deInterpreter interpreter) {
    dePopped condition;
    char**   ifbody;
    char**   elsebody;
    if(interpreter.Program == deNil) {
        return interpreter;
    }
    ifbody = interpreter.Program;
    while(*interpreter.Program                          == deNil &&
          deStringCompare("else", *interpreter.Program) == false) {
        interpreter.Program++;
    }
    if(*interpreter.Program == deNil) {
        interpreter.Program = deNil;
        return interpreter;
    }
    interpreter.Program = ifbody;
    while(*interpreter.Program                          == deNil &&
          deStringCompare("then", *interpreter.Program) == false) {
        interpreter.Program++;
    }
    if(*interpreter.Program == deNil) {
        interpreter.Program = deNil;
        return interpreter;
    }
    interpreter.Program = ifbody + 1;
    condition           = dePop(interpreter.Stack);
    interpreter.Stack   = condition.Stack;
    ifbody              = deNil;
    elsebody            = deNil;
    if(condition.Value) {
        while(deStringCompare("else", *interpreter.Program) == false) {
            ifbody = deTokensAppend(*interpreter.Program, ifbody);
            interpreter.Program++;
        }
        elsebody            = interpreter.Program;
        interpreter.Program = ifbody;
        interpreter         = deExecute(interpreter);
        interpreter.Program = elsebody;
        while(deStringCompare("then", *interpreter.Program) == false) {
            interpreter.Program++;
        }
        deRelease(ifbody);
    }
    else {
        while(deStringCompare("else", *interpreter.Program) == false) {
            interpreter.Program++;
        }
        interpreter.Program++;
        while(deStringCompare("then", *interpreter.Program) == false) {
            elsebody = deTokensAppend(*interpreter.Program, elsebody);
            interpreter.Program++;
        }
        ifbody              = interpreter.Program;
        interpreter.Program = elsebody;
        interpreter         = deExecute(interpreter);
        interpreter.Program = ifbody;
        deRelease(elsebody);
    }
    return interpreter;
}

deInterpreter deDefineStandardWords(deInterpreter interpreter) {
    interpreter.Dictionary = deDefine("exit", false, deNil,           deExit,
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("(",    false, deNil,           deComment,
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("def",  false, deNil,           deCompileWord,
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("+",    false, deAdd,           deNil, 
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("-",    false, deSubtract,      deNil, 
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("*",    false, deMultiply,      deNil, 
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("/",    false, deDivide,        deNil,
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("&&",   false, deAND,           deNil,
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("||",   false, deOR,            deNil, 
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("!!",   false, deNOT,           deNil, 
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("<",    false, deLessThan,      deNil, 
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine(">",    false, deGreaterThan,   deNil, 
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("<=",   false, deLessThanEq,    deNil, 
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine(">=",   false, deGreaterThanEq, deNil, 
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("==",   false, deEquals,        deNil, 
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("if",   false, deNil,           deIf, 
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("pick", false, dePick,          deNil, 
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("roll", false, deRoll,          deNil, 
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("drop", false, deDrop,          deNil, 
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("dup",  false, deDuplicate,     deNil, 
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("over", false, deOver,          deNil, 
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("swap", false, deSwap,          deNil, 
                                      deNil, interpreter.Dictionary);
    interpreter.Dictionary = deDefine("rot",  false, deRotate,        deNil, 
                                      deNil, interpreter.Dictionary);
    return interpreter;
}

deInterpreter deExecute(deInterpreter interpreter) {
    deWord word;
    char** temporary;
    deWord dictionary;
    char** program;
    if(interpreter.Program         == deNil ||
       interpreter.Dictionary.Bank == deNil) {
        return interpreter;
    }
    temporary = interpreter.Program;
    while(*interpreter.Program != deNil) {
        word = deFind(*interpreter.Program, interpreter.Dictionary);
        if(word.Compile != deNil) {
            interpreter = word.Compile(interpreter);
            if(interpreter.Program == deNil) {
                interpreter.Program = temporary;
                return interpreter;
            }
        }
        else if(word.Code                         != deNil &&
                deIsNumeric(*interpreter.Program) == false) {
            dictionary             = interpreter.Dictionary;
            program                = interpreter.Program;
            interpreter.Dictionary = word;
            interpreter.Program    = word.Code;
            interpreter            = deExecute(interpreter);
            interpreter.Dictionary = dictionary;
            interpreter.Program    = program;
        }
        else if(word.Runtime != deNil) {
            interpreter.Stack = word.Runtime(interpreter.Stack);
        }
        else {
            if(deIsNumeric(*interpreter.Program) == true) {
                interpreter.Stack = dePush(deToNumeric(*interpreter.Program),
                                           interpreter.Stack);
            }
            else {
                interpreter.Program = temporary;
                return interpreter;
            }
        }
        interpreter.Program++;
    }
    interpreter.Program = temporary;
    return interpreter;
}

deInterpreter deRunProgram(char* program, deInterpreter interpreter) {
    if(program == deNil) {
        return interpreter;
    }
    if(interpreter.Program != deNil) {
        deRelease(*interpreter.Program);
        deRelease(interpreter.Program);
    }
    interpreter.Program = deTokenizeString(program);
    interpreter         = deExecute(interpreter);
    return interpreter;
}

deInterpreter deDeleteInterpreter(deInterpreter interpreter) {
    if(interpreter.Program != deNil) {
        deRelease(*interpreter.Program);
        deRelease(interpreter.Program);
    }
    interpreter.Dictionary = deDeleteDictionary(interpreter.Dictionary);
    interpreter = deNewInterpreter(deNil, 0, deNil, 0, deNil, 0, deNil);
    return interpreter;
}
